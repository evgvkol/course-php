<?php

//-== Домашнее задание у четвергу ==-
//Создать класс MobileOperator
//	свойства
//		protected $phones // массив из объектов класса Phone
//	методы
//		public addPhone($number) // принимает номер телефона, создает объект класса Phone и добавляет его в массив $this->phones
//		public findByNumber($number) // метод поиска объекта в массиве $this->phones по номеру телефона (foreach по $this->phones as $phone и если найден такой $phone->getNumber() == $number то return $phone, иначе return false)


class MobileOperator {
    
    protected $phones = array();
    
    public function addPhone($number){
      $this->phones[] = New Phone ($this, $number);
    }
    
    public function findByNumber ($number){
      foreach ($this->phones as $phone){
          if ($phone->getNumber() == $number){
              return $phone;
          }
      }
      return FALSE;
    }
}
//
//Создать класс Phone
//	свойства
//		protected $mobileOperator; // объект класса MobileOperator
//		protected $number;
//	методы: 
//		public call($number) // позвонить
//		public accept($phone) // принять вызов. $phone объект класса Phone
//		public getNumber() // возвращает номер телефона
//		public setNumber($number) // устанавливает номер телефона
//
class Phone {
    protected $mobileOperator;
    protected $number;

//	Метод call($number)
//		Выполняет метод $phone = $this->mobileOperator->findByNumber($number) и либо получает объект класса Phone, либо false
//		Если объект найден, то делаем $phone->accept()
//		Если не найден (false), то пишем 'Телефон такой-то не найден'  
    public function call($number){ // позвонить
        $phone = $this->mobileOperator->findByNumber($number);
        if ($phone){
            $phone->accept($phone);
        } 
        else {
            echo 'Телофон '.$number.' не найден'.PHP_EOL;
        }
    }
//
//	Метод accept($phone)
//		Просто выводит текст 'Поступил входящий звонок с номера ' . $phone->getNumber()
    
    public function accept($phone){ // принять вызов. $phone объект класса Phone
       echo'Поступил входящий звонок с номер '.$phone->getNumber().PHP_EOL; 
    }
    
    public function getNumber(){ // возвращает номер телефона
       return $this->number; 
    }
    
    public function setNumber($number){ // устанавливает номер телефона
       return $this->number = $number; 
    }
   
    //В конструкторе Phone принимать аргумент $mobileOperator и $number и 
    //устанавливать в свойства $this->mobileOperator и $this->number(через метод
    // $this->setNumber)
    public function __construct($mobileOperator, $number) {
        $this->mobileOperator = $mobileOperator;
        $this->number = $this->setNumber($number);
    }
}

$filename = 'phonenum';

//Создать массив $numbers и сразу заполнить его несколькими номерами телефонов
$numbers = array('89236789889',
                 '89236789890',
                 '89236789891',
                 '89236789892',
                 '89236789893',
                 '89236789894');

// $myPhone = '89236789891';

////Создать объект $megafon класса MobileOperator 
$megafon = New MobileOperator();

//и в цикле foreach по $numbers вызывать у $megafon метод addPhone для добавления телефонов
foreach ($numbers as $number){
    $megafon->addPhone($number);
}

//Теперь все готово к тому, чтобы запустить и проверить. Для этого пишем код,
// выполнящий следующее действие
//Находим объект класса Phone используя один из имеющихся телефонов (любой) 
//через $megafon->findByNumber(номер телефона) и помещаем в любую переменную, 
//к примеру $myPhone
$myPhone = $megafon->findByNumber ('89236789892'); // Ищем существующий телефон 

//У этой переменной вызываем метод $myPhone->call(тут передаем номер телефона, 
//который так же есть в списке) и получаем информацию о том, что поступил входящий звонок.
$myPhone->call('89236789893');

//После этого так же делаем $myPhone->call(и тут любой номер, которого нет в списке)
// и получаем информацию о том, что телефон такой-то не найден
$myPhone->call('89236789895');