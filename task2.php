<?php

// Самостоятельная работа. Задание 1
//function create_fio($name, $family, $year) {
//    $fio = array($name, $family, $year);
//    return $fio;
//}

/* $fio = create_fio('Евгений', 'Колесников', 1976);
var_dump($fio);
echo PHP_EOL;

// Самостоятельная работа. Задание 2
$fio1 = array(); // создаем новый массив для принятия данных

function create_fio1($arg = array(), &$arr) { // принимаем значения массива, принимаем ссылку на пустой массив
    // $arr[0] = $arg[0]; // 1 вариант
    // $arr[1] = $arg[1];
    // $arr[2] = $arg[2];
    $arr = $arg; // 2 вариант
}

create_fio1($fio, $fio1);
var_dump($fio1);
*/

// Первая функция передаем 3 параметра, возвращает объект содержащий 3 свойства
$about = (object) array (
    'first_name'    => null,
    'last_name'     => null,
    'year'          => null
);

function setObject ($first_name,$last_name,$year) {
    $about->first_name  = $first_name;
    $about->last_name   = $last_name;
    $about->year        = $year;
    return $about;
}

$about = setObject('Evgeny','Kolesnikov',1976);
var_dump($about);

// Вторая функция принимает этот объект и изменяет на какието другие значения

function setInsert ($data){
    $data->first_name++;
    $data->last_name++;
    $data->year++;
    return $data;
}

var_dump(setInsert($about));
