<?php

class PersonalData { // Создаем клас
  
  public $name; // Имя
  public $lastname;  // Фамилие
  protected $age;  // возраст protected

  // Устанавливаем через конструктор name и lastname
  public function __construct($name, $lastname) {
    $this->name = $name;
    $this->lastname = $lastname;
  }
  
  // создаем два метода SetAge и GetAge  
  public function SetAge($age){  // Устанавливаем возраст
    $this->age = $age;
  }
  public function GetAge(){  // Получае возраст
    return $this->age;
  }
}

$obj = New PersonalData('Evgeny', 'Kolesnikov'); // Создаем объект этого класса