<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
    
        public function __construct() {
            parent::__construct();
            
            $this->load->model('Users_Model');
        }

        public function index() {     
            if ($this->user) {
                $this->redirect('/index.php/welcome');
            }
            
            $err = false;
            if ($_POST) {
                $login = $_POST['login'];
                $password = $_POST['password'];
                $user = Users_Model::getForLoginPassword($login, $password);
                if (!$user) {
                    $err = 'Неверный логин или пароль';
                } else {
                    setcookie('user_id', $user->id, 0, '/');
                    $this->redirect('/index.php/welcome');
                }
            }
            
            $this->render('login', array(
                'err' => $err
            ));            
        }
        
        public function quit() {
            setcookie('user_id', 0, 0, '/');
            $this->redirect('/index.php/login');
        }
}