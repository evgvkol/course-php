<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {
    
        public function __construct() {
            parent::__construct();
            
            if (!$this->user) {
                $this->redirect('/index.php');
            }
        }

        public function index() {
            if ($_POST) {
                $id = $_POST['id'];
                $fname = $_POST['first_name'];
                $lname = $_POST['last_name'];
                
                $error = Users_Model::make($id, $fname, $lname);
            }
            
            $obj = new Users_Model();
            $list = $obj->getList();
            
            $this->render('users', array(
                'list' => $list,
                'error' => isset($error) ? $error : null
            ));
        }
        
        public function remove() {
            $id = isset($_GET['id']) ? $_GET['id'] : null;
            Users_Model::remove($id);
            
            $this->redirect('/index.php/welcome');
        }
        
        public function edit() {
            if ($_POST && isset($_POST['user'])) {
                foreach ($_POST['user'] as $field => $value) {
                    if (!$value) {
                        continue;
                    } elseif ($field == 'password') {
                        $this->user->$field = md5($value);
                    } else {
                        $this->user->$field = $value;
                    }
                }
                
                Users_Model::save($this->user);
            }
            
            $this->render('user_edit', array(
                'user' => $this->user
            ));
        }
        
        public function test() {
            var_dump($_POST);
        }
    
}