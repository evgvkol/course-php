<?php

class Messages_Model extends CI_Model {
    
    public $id;
    public $user_id;
    public $text;
    
    // получение даных таблицы messages
    public function getList() {
        $query = $this->db->get('messages');
        return $query->result();
    }
    
    // получение данных таблицы users 
    public function getuserid() {
        $query = $this->db->get('users');
        return $query->result();
    }
    
    public static function remove($id) {
        if (!$id) {
            return false;
        }
        $obj = new self;
       // var_dump($obj,users);
        $obj->db->delete('messages', array('id'=>$id));
    }
    
    public static function make($id, $user_id, $text) {
        if (!$id || !$user_id || !$text) {
            return array(
                'error' => 1,
                'msg' => 'Введите все данные'
            );
        }
        
        $obj = new self;
        $obj->id = $id;
        $obj->user_id = $user_id;
        $obj->text = $text;
        
        $res = $obj->db->insert('messages', $obj);
        
        return array('error'=>0);
    }
    
//     public static function make($id, $user_id, $text) {
//        if (!$id || !$user_id || !$text) {
//            return array(
//                'error' => 1,
//                'msg' => 'Введите все данные'
//            );
//        }
//        
//        $obj = new self;
//        $obj->id = $id;
//        $obj->user_id = $user_id;
//        $obj->text = $text;
//        
//        $res = $obj->db->insert('messages', $obj);
//        
//        return array('error'=>0);
//    }
}
